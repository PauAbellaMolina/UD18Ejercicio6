import dto.*;
import methods.*;

public class Ejercicio6App {
	public static void main(String[] args) {
		Database newDatabase = new Database("UD18Ejercicio6");
		Table Piezas = new Table("UD18Ejercicio6", "Piezas");
		Table Proveedores= new Table("UD18Ejercicio6", "Proveedores");
		Table Suministra = new Table("UD18Ejercicio6", "Suministra");
		openConnection.openConnection();
		createDB.createDB(newDatabase);
		createTable.createTable(Piezas, " (`Codigo` INT NOT NULL AUTO_INCREMENT, `Nombre` NVARCHAR(100) NULL, PRIMARY KEY (`Codigo`));");
		createTable.createTable(Proveedores, " (`Id` CHAR(4) NOT NULL, `Nombre` NVARCHAR(100) NULL, PRIMARY KEY (`Id`));");
		createTable.createTable(Suministra, " (`CodigoPieza` INT NOT NULL, `IdProveedor` CHAR(4) NOT NULL, `Precio` INT NULL, PRIMARY KEY (`CodigoPieza`, `IdProveedor`), INDEX `IdProveedor_idx` (`IdProveedor` ASC) VISIBLE, CONSTRAINT `CodigoPieza` FOREIGN KEY (`CodigoPieza`) REFERENCES `UD18Ejercicio6`.`Piezas` (`Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT `IdProveedor` FOREIGN KEY (`IdProveedor`) REFERENCES `UD18Ejercicio6`.`Proveedores` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION);");
		insertData.insertData("UD18Ejercicio6", "Piezas", "(Nombre) VALUE('Tuerca');");
		insertData.insertData("UD18Ejercicio6", "Piezas", "(Nombre) VALUE('Rosca');");
		insertData.insertData("UD18Ejercicio6", "Piezas", "(Nombre) VALUE('Airbag');");
		insertData.insertData("UD18Ejercicio6", "Piezas", "(Nombre) VALUE('Broca');");
		insertData.insertData("UD18Ejercicio6", "Piezas", "(Nombre) VALUE('Rodamiento');");
		insertData.insertData("UD18Ejercicio6", "Proveedores", "(Id, Nombre) VALUE('BH', 'Bosch');");
		insertData.insertData("UD18Ejercicio6", "Proveedores", "(Id, Nombre) VALUE('BD', 'Black Decker');");
		insertData.insertData("UD18Ejercicio6", "Proveedores", "(Id, Nombre) VALUE('HI', 'Hitachi');");
		insertData.insertData("UD18Ejercicio6", "Proveedores", "(Id, Nombre) VALUE('MA', 'Makita');");
		insertData.insertData("UD18Ejercicio6", "Proveedores", "(Id, Nombre) VALUE('DW', 'DeWalt');");
		insertData.insertData("UD18Ejercicio6", "Suministra", "(CodigoPieza, IdProveedor, Precio) VALUE(1, 'BH', 10);");
		insertData.insertData("UD18Ejercicio6", "Suministra", "(CodigoPieza, IdProveedor, Precio) VALUE(2, 'BD', 100);");
		insertData.insertData("UD18Ejercicio6", "Suministra", "(CodigoPieza, IdProveedor, Precio) VALUE(3, 'HI', 1);");
		insertData.insertData("UD18Ejercicio6", "Suministra", "(CodigoPieza, IdProveedor, Precio) VALUE(4, 'MA', 15);");
		insertData.insertData("UD18Ejercicio6", "Suministra", "(CodigoPieza, IdProveedor, Precio) VALUE(5, 'DW', 24);");
		closeConnection.closeConnection();
	}
}
